<?php

include_once 'postavke.php';

class AjaxApi {

    private $korisnicko_ime;
    private $email;
    private $lozinka;
    private $potvrda_lozinke;
    private $funkcija;
    private $podaci;

    public function __construct(){}

    public function prihvatPodataka($podaci = ''){

      $this->funkcija = $podaci['upit'];

     if($this->dozvoljenaFunkcija()){

      $this->podaci = $podaci;

      $this->pokreni($this->funkcija);

      } else {
         echo 'Došlo je do pogreške kontaktirajte administratora';
      }

    }


    private function dozvoljenaFunkcija(){
      $funkcije_klase = get_class_methods(new ajaxAPI());
      return in_array($this->funkcija, $funkcije_klase);
      
    }

    public function pokreni($pokreni_funkciju){
      $this->$pokreni_funkciju();
   }

    private function registrirajKorisnika(){
      $korisnik = new AutentikacijaKorisnika();
      $korisnik->procesiarj($this->podaci,$this->funkcija);
   }

   private function prijavaKorisnika(){
      $korisnik = new AutentikacijaKorisnika();
      $korisnik->procesiarj($this->podaci,$this->funkcija);
   }

   private function filmovi(){

      if($this->podaci['podaci']['vrsta_upita'] === 'dohvati_medije_big'){
         $filmovi = new PrikazFilmova();
         $filmovi->podaci_filma();
      }

      if($this->podaci['podaci']['vrsta_upita'] === 'unos_medija'){
         $filmovi = new UnosMedijskihPodataka();
         $filmovi->forma_unosa_filma();
      }
     

    //  $filmovi->podaci_filma();
     // print_r($this->podaci);
   //   print_r($this->funkcija);
   }


   public function imdb(){

      if($this->podaci['podaci']['vrsta_upita'] === 'imdb_unos'){
         $imdb_unos = new UnosMedijskihPodataka();
         $imdb_unos->dodaj_imdb_u_bazu($this->podaci['podaci']['ime_filma']);
      }

      if($this->podaci['podaci']['vrsta_upita'] === 'imdb_prikaz'){
         $imdb_unos = new UnosMedijskihPodataka();
         $imdb_unos->prijedlog_filma_za_unos($this->podaci['podaci']);
      }
      
      
   }



public function glavni_dashboard(){
   include_once PUTANJA . 'app/admin/template-parts/main-dashboard.php';
}












   /*


   private function filmovi(){
      $unesi_film = new UnosMedijskihPodataka();
      $unesi_film->podaci($this->podaci,$this->funkcija);
      
   }

   public function svi_filmovi(){
      $filmovi = new PrikazFilmova();
      $filmovi->podaci_filma();
   }

   public function imdb(){
      $imdb_unos = new unosFilmova();
      $imdb_unos->prijedlog_filma_za_unos($this->podaci['podaci']);
   }

   public function unos(){
      $imdb_unos = new unosFilmova();
      $imdb_unos->forma_unosa_filma();
   }

   public function film_meta(){
       $filmovi = new PrikazFilmova();
      $filmovi->popup_film($this->podaci['podaci']);
   }

   public function uredi_medij(){
      $uredi_medij = new UrediMedij();
      $uredi_medij->prikazi_html();
      $uredi_medij->dohvati_medije();
   
   }

   public function uredi_medij_json(){
      $uredi_medij = new UrediMedij();
      $uredi_medij->dohvati_medije();
   }

   public function promjena_statusa(){
     
   }



   public function dodaj_imdb_u_bazu(){
      
      include_once PUTANJA . 'app/dbz.php';
      include_once PUTANJA . 'app/filmovi/imdb.class.php';

      //print_r($this->podaci['$imefilma']);

      $ime_filmova = $this->podaci['podaci']['ime_filma'];

         $IMDB = new IMDB($ime_filmova);
         if ($IMDB->isReady) {

       $slika = $IMDB->getPoster($sSize = 'small', $bDownload = false);
       $opis = $IMDB->getDescription();
       $zanr_filma = $IMDB->getGenre();
       $plot = $IMDB->getPlot($iLimit = 0);
       $rating = $IMDB->getRating();
       $trailer = $IMDB->getTrailerAsUrl($bEmbed = false);
       $ime = $IMDB->getTitle($bForceLocal = true);
       $godina = $IMDB->getYear();
       $godina_izlaska = $IMDB->getReleaseDate();
       $duljina = $IMDB->getRuntime();

       $ch = curl_init ($slika);
       $this->download_image1($slika, $ime .'.jpg');
       $this->imdb_unos = new dbz();
       $this->imdb_unos->imeTablice('imdb_podaci_filmova');
       $film_postoji_imdb = $this->imdb_unos->findOne((['ime_filma' => $ime]));
       
      if(!$film_postoji_imdb){
         $this->imdb_unos->dodaj([
            'ime_filma' => $ime,
            'slika_filma' => $slika,
            'opis_filma' =>  $opis,
            'zanr_filma' => $zanr_filma,
            'plot_filma' => $plot,
            'ocijena_filma' => $rating,
            'datum_izlaska_filma' => $godina_izlaska,
            'duljina_filma' => $duljina,
            'trailer_filma' => $trailer,
            'godina_izlaska_filma' => $godina
    
          ]);
          $zadnji_id = $this->imdb_unos->lastSavedId();
          echo json_encode(array('imdb_id' => $zadnji_id));
      } else {
         echo json_encode(array('imdb_id' => $film_postoji_imdb['id']));
      }
    

  }

}


public function download_image1($image_url, $image_file){

   $direktorij = PUTANJA . 'uploads/imdb';
   $file = fopen(DOWNLOAD . $image_file, 'w');
   // cURL
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $image_url);
   // set cURL options
   curl_setopt($ch, CURLOPT_VERBOSE, 1);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_AUTOREFERER, false);
   curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
   curl_setopt($ch, CURLOPT_HEADER, 0);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // <-- don't forget this
   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // <-- and this
   curl_setopt($ch, CURLOPT_FAILONERROR, true);
   curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
   // set file handler option
   curl_setopt($ch, CURLOPT_FILE, $file);
   // execute cURL
   curl_exec($ch);
   // close cURL
   curl_close($ch);
   // close file
   fclose($file);                                 // closing file handle
}

*/
   
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_POST['upit'])) {
   $korisnik = new AjaxApi();
   $korisnik->prihvatPodataka($_POST);
   
} 

