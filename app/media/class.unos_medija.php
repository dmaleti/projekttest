<?php

class UnosMedijskihPodataka{

public function __construct(){

    if($_SESSION["id"]){
        return;
    }

    //TODO authentikacija, dali korisnih ima pravo unostiti medije

    $lvl_pristupa[] = '1, 2, 3';

    if(in_array('6',$lvl_pristupa)){
        echo 'Nemate pristup';
        return;
    }

    $this->unos = new dbz();
    $this->autor  = $_SESSION["id"];
    }

    // Automatski pokreni funkciju
    public function vrsta_unosa($pokreni_funkciju){
        $this->$pokreni_funkciju();
    }

    public function unesi_u_bazu($podaci,$upit){
        $this->naziv  = $podaci['podaci']['ime_filma'] ? $podaci['podaci']['ime_filma'] : '';
        $this->link   = $podaci['podaci']['link_filma'] ? $podaci['podaci']['link_filma'] : '';
        $this->imdb_id   = $podaci['podaci']['imdb'] ? $podaci['podaci']['imdb'] : '';
        $this->vrsta_poziva   = $podaci['podaci']['vrsta'];
        $this->tablica = $podaci['upit'];
        $this->vrsta_unosa($podaci['upit']);

    }
    public function prijedlog_filma_za_unos($ime_filma){

        // print_r($ime_filma);
       $this->imdb_popis = new dbz();
       $this->imdb_popis->imeTablice('imdb_podaci_filmova');
       $film_postoji_imdb = $this->imdb_popis->prijedlog($ime_filma['ime_filma']);
 
     if($film_postoji_imdb){
 
     foreach($film_postoji_imdb as $film){
         echo '
         <a class="add-imdb row" data-imdb="'.$film["id"].'">
         <div class="slika-mala col-md-2"><img src="'.$film['slika'].'" /></div>
         <div class="info-imdb col-md-10">
         <h3>'.$film['naziv'].'</h3>
         <ul class="movie-det">
             <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" style="fill:#ffec00"/></svg><span>'.$film['ocijena'].'</span></li>
                    <li>'.$film['godina_izlaska'].'</li>
                    <li>'.$film['datum_izlaska'].'</li>
        </ul>
            <span class="zanr">'.$film['zanr'].'</span>
         </div>
         
         </a>
         
         ';
     }
 
 } else {
    $IMDB = new IMDB($ime_filma['ime_filma']);
    if ($IMDB->isReady) {
       $slika = $IMDB->getPoster($sSize = 'small', $bDownload = false);
       $opis = $IMDB->getDescription();
       $zanr_filma = $IMDB->getGenre();
       $plot = $IMDB->getPlot($iLimit = 0);
       
       $trailer = $IMDB->getTrailerAsUrl($bEmbed = false);
       $ime = $IMDB->getTitle($bForceLocal = true);
       $godina = $IMDB->getYear();
       $rating = $IMDB->getRating();
       $godina = $IMDB->getYear();
       $duljina = $IMDB->getRuntime();
       $zanr_filma = $IMDB->getGenre();

         echo '
         <a class="add-imdb" data-imdb="0">
         <div class="slika-mala"><img src="'.$slika.'" /></div>
         <div class="info-imdb">
         <h3>'.$ime.'</h3>
         <ul class="movie-det">
             <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" style="fill:#ffec00"/></svg><span>'.$rating.'</span></li>
                    <li>'.$godina.'</li>
                    <li>'.$duljina.'</li>
        </ul>
            <span class="zanr">'.$zanr_filma.'</span>
         </div>
         
         </a>
         
         ';
       
    } else {
       echo 'Movie not found. 😞';
   }
 }
    
 
 
     }



    public function forma_unosa_filma(){
        ?>

<div class="row">
    <div class="col-sm-12 col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="clipboaard-container">
                <div class="col-sm-12 col-md-6">
                    <form class="form-horizontal">
                    <fieldset>
                    <!-- Search input-->
                    <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-left" for="naziv">Ime Filma</label>
                    <div class="col-lg-12">
                        <input id="naziv" name="naziv" type="search" placeholder="Tražite po imenu ili unesite IMDB kod" class="form-control btn-square input-md">
                        <input type="hidden" class="imdb-id" value="0">
                        <span style="display:none;" class="help-block"></span>
                        <div class="ajax"></div>
                        <p class="help-block">help</p>
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-left" for="link-photo">Google photo link</label>  
                    <div class="col-lg-12">
                    <input id="link-photo" name="link-photo" type="text" placeholder="" class="form-control btn-square input-md">
                    <p class="help-block">help</p>  
                    </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-left" for="link-drive">Google drive link</label>  
                    <div class="col-lg-12">
                    <input id="link-drive" name="link-drive" type="text" placeholder="" class="form-control btn-square input-md">
                    <p class="help-block">help</p>  
                    </div>
                    </div>
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Unesi</button>

                    </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div> 
        
             <div class="user-data col-sm-12 col-xl-6"></div>
</div>
        <?php
    }



    public function dodaj_imdb_u_bazu($ime_filmova){
      
        include_once PUTANJA . 'app/dbz.php';
        include_once PUTANJA . 'app/filmovi/imdb.class.php';
  
        //print_r($this->podaci['$imefilma']);
  
        
  
           $IMDB = new IMDB($ime_filmova);
           if ($IMDB->isReady) {
  
         $slika = $IMDB->getPoster($sSize = 'big', $bDownload = true);
         $opis = $IMDB->getDescription();
         $zanr_filma = $IMDB->getGenre();
         $plot = $IMDB->getPlot($iLimit = 0);
         $rating = $IMDB->getRating();
         $trailer = $IMDB->getTrailerAsUrl($bEmbed = false);
         $ime = $IMDB->getTitle($bForceLocal = true);
         $godina = $IMDB->getYear();
         $godina_izlaska = $IMDB->getReleaseDate();
         $duljina = $IMDB->getRuntime();
  
       //  $ch = curl_init ($slika);
        // $this->download_image1($slika, $ime .'.jpg');
         $this->imdb_unos = new dbz();
         $this->imdb_unos->imeTablice('imdb_podaci');
         $film_postoji_imdb = $this->imdb_unos->findOne((['naziv' => $ime]));
         
        if(!$film_postoji_imdb){
           $this->imdb_unos->dodaj([
              'naziv' => $ime,
              'slika' => $slika,
              'opis' =>  $opis,
              'zanr' => $zanr_filma,
              'plot' => $plot,
              'ocijena' => $rating,
              'datum_izlaska' => $godina_izlaska,
              'duljina' => $duljina,
              'trailer' => $trailer,
              'godina_izlaska' => $godina
      
            ]);
            $zadnji_id = $this->imdb_unos->lastSavedId();
            echo json_encode(array('imdb_id' => $zadnji_id));
        } else {
           echo json_encode(array('imdb_id' => $film_postoji_imdb['id']));
        }
      
  
    }
  
  }

}