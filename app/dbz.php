<?php

class dbz {

    private $dbconnection = false;

    private $dbuser = 'root';
    private $dblozinka = '';
    private $dbbaza = 'projekt';
    private $dbhost = 'localhost';

    private $parameters;

    private $stmt;
    private $data = array();
    private $sql;
    private $where;
    private $fields;
    private $count;
    private $fetch;
    private $lastId;





    public function __construct()
    {
        $this->Connect();
        $this->parameters = array();
      
    }

    private function Connect()
    {
        $dsn = 'mysql:dbname=' . $this->dbbaza . ';host=' . $this->dbhost . '';
        try {
            # Read settings from INI file, set UTF8
            $this->pdo = new PDO($dsn, $this->dbuser, $this->dblozinka, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));
            
            # We can now log any exceptions on Fatal error. 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            # Disable emulation of prepared statements, use REAL prepared statements instead.
            //$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            
            # Connection succeeded, set the boolean to true.
            $this->dbconnection = true;
        }
        catch (PDOException $e) {
            # Write into log
            throw new Exception($e->getMessage());
            
            die();
        }
    }

    public function dodaj($data)
    {
        $this->data = $data;
        $this->stmt = $this->pdo->prepare(self::insertQueryString());
        self::param($data);
        $this->stmt->execute();
        $this->count = $this->stmt->rowCount();
    }

    private function insertQueryString()
    {
        $fields = self::fields($this->data);
        $values = self::values();

        return "INSERT INTO {$this->table} ({$fields}) VALUES ({$values})";
    }

    private function param($data = null)
    {
        if (empty($data)) {
            $data = $this->data['conditions'];
        }

        foreach ($data as $k => $v) {
            $tipo = (is_int($v)) ? PDO::PARAM_INT : PDO::PARAM_STR;
            $this->stmt->bindValue(":{$k}", $v, $tipo);
        }
    }
    private function fields($data = null)
    {
        if (empty($data) && isset($this->data['fields'])) {
            return implode(',', $this->data['fields']);
        }

        if ( ! empty($data)) {
            foreach ($data as $k => $v) {
                $fields[] = $k;
            }
            return implode(',', $fields);
        }

        return '*';
    }
    private function values()
    {
        foreach ($this->data as $k => $v) {
            $values[] = ":{$k}";
        }

        return implode(',', $values);
    }

    public function imeTablice($tablica){
        $this->table = $tablica;
    }

    private function find()
    {
        $sql = "SELECT " . self::fields() . " FROM {$this->table} " . self::where();

        $this->stmt = $this->pdo->prepare($sql);

        if ( ! empty($this->where)) {
            self::param();
        }

        $this->stmt->execute();
        return $this;
    }
    public function findById($id)
    {
        return self::findOne([$this->pk => $id]);
    }

    public function pass_vrfy($ime){

      
    }

    public function findOne($data)
    {
        $this->data['conditions'] = $data;
        return $this->fetch = $this->find()
                                    ->stmt->fetch(PDO::FETCH_ASSOC);
    }
    private function where()
    {
        return $this->where = (isset($this->data['conditions']))
                              ? 'WHERE ' . self::conditions(' AND ')
                              : '';
    }

    public function fetch()
    {
        return $this->fetch;
    }
    public function findAll($data = null)
    {
        $this->data = $data;
        return $this->find()
                    ->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    private function conditions($separator)
    {
        $param = [];
        foreach ($this->data['conditions'] as $k => $v) {
            $param[] = "{$k} = :{$k}";
        }

        return implode($separator, $param);
    }

    public function lastSavedId()
    {
        $id = $this->pdo->lastInsertId();
        return ($id) ? $id : $this->lastId;
    }

    public function korisnicki_meta_podaci($korisnik_id,$kljuc){

        $query = "SELECT vrijednost_podatka FROM korisnicki_podaci WHERE korisnik_id = :korisnik_id AND kljuc_podataka = :kljuc_podataka";

        $stmt = $this->pdo->prepare($query);

        $stmt->bindValue(':korisnik_id', $korisnik_id);
        $stmt->bindValue(':kljuc_podataka', $kljuc);

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function prijedlog($naziv){

        $query = "SELECT * FROM imdb_podaci WHERE naziv LIKE :naziv";
    

        $stmt = $this->pdo->prepare($query);

        $stmt->bindValue(':naziv', '%'.$naziv.'%');

      //  $stmt->bindParam(':term', $term, PDO::PARAM_STR);

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function film_sa_meta_podacima($autor,$id){

        $sql = "SELECT *
        FROM filmovi a
        INNER JOIN podaci_filmova b
        ON a.id=b.film_id
        INNER JOIN imdb_podaci_filmova c
        ON b.vrijednost_podatka=c.id
        WHERE a.autor_posta ='$autor' AND a.id = '$id'  ORDER BY datum_unosa DESC LIMIT 21";

$stmt = $this->pdo->prepare($sql);

//Execute.
$stmt->execute();

//Fetch row.
return $stmt->fetchAll(PDO::FETCH_ASSOC);


    }


    public function dohvati_medije_big($autor,$vrsta,$meta_vrsta){

    $sql = "SELECT *
            FROM $vrsta a
            INNER JOIN podaci_$meta_vrsta b
            ON a.id=b.film_id
            INNER JOIN imdb_podaci c
            ON b.vrijednost_podatka=c.id
            WHERE a.autor_posta = :autor ORDER BY datum_unosa DESC LIMIT 21";

    $stmt = $this->pdo->prepare($sql);
    
    //Bind value.
    $stmt->bindValue(':autor', $autor);
    
    //Execute.
    $stmt->execute();
    
    //Fetch row.
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }



    public function testing(){

        // sql to create table
        $sql = "CREATE TABLE filmovi (
           id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
           ime_filma varchar(60) NOT NULL,
           g_link varchar(255) NOT NULL,
           hash_link varchar(100) NOT NULL,
           status int(11), 
           reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
           )";
         
           // use exec() because no results are returned
           $this->pdo->exec($sql);
           echo "Table MyGuests created successfully";
   }

}

