<?php

$router = new Router();
// $rsouter = new unos_filmova();
 // Custom 404 Handler
 $router->set404(function () {
     header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
     echo '404, route not found!';
 });

 // Before Router Middleware
 $router->post('ajaxapi', '/.*', function () {
    require_once PUTANJA . 'app/AjaxApi.php';
 });

 // Static route: / (homepage)
 $router->get('/', function () {
     echo '<h1>bramus/router</h1><p>Try these routes:<p><ul><li>/hello/<em>name</em></li><li>/blog</li><li>/blog/<em>year</em></li><li>/blog/<em>year</em>/<em>month</em></li><li>/blog/<em>year</em>/<em>month</em>/<em>day</em></li><li>/movies</li><li>/movies/<em>id</em></li></ul>';
 });

 // Static route: /hello
 $router->get('/hello', function () {
     echo '<h1>bramus/router</h1><p>Visit <code>/hello/<em>name</em></code> to get your Hello World mojo on!</p>';
 });

  // Static route: /hello
  $router->get('/administracija', function () {
     require_once PUTANJA . 'app/admin/administracija.php';
 });

 $router->get('/odjava', function () {

     // Initialize the session
session_start();
// Unset all of the session variables
$_SESSION = array();
// Destroy the session.
session_destroy();
header("location:" . URLSTR . "prijava");
     exit;

 });

 // Static route: /hello
    $router->get('/prijava', function () {

 require_once PUTANJA . 'public/registracija.php';

    });

    $router->get('/registracija', function () {

     require_once PUTANJA . 'public/registracija.php';
 
        });
        $router->get('/resetiraj-lozinku', function () {

         require_once PUTANJA . 'public/registracija.php';
     
            });
 // Dynamic route: /hello/name
 $router->get('/hello/(\w+)', function ($name) {
     echo 'Hello ' . htmlentities($name);
 });


 $router->get('/dashboard/(\w+)', function ($name) {
    if(htmlentities($name) == 'filmovi'){
        require_once PUTANJA . 'administracija.php';
    }
    if(htmlentities($name) == 'dodajfilm'){
        require_once PUTANJA . 'administracija.php';
        
    }
});






 // Dynamic route: /ohai/name/in/parts
 $router->get('/ohai/(.*)', function ($url) {
     echo 'Ohai ' . htmlentities($url);
 });

 // Dynamic route with (successive) optional subpatterns: /blog(/year(/month(/day(/slug))))
 $router->get('/blog(/\d{4}(/\d{2}(/\d{2}(/[a-z0-9_-]+)?)?)?)?', function ($year = null, $month = null, $day = null, $slug = null) {
     if (!$year) {
         echo 'Blog overview';

         return;
     }
     if (!$month) {
         echo 'Blog year overview (' . $year . ')';

         return;
     }
     if (!$day) {
         echo 'Blog month overview (' . $year . '-' . $month . ')';

         return;
     }
     if (!$slug) {
         echo 'Blog day overview (' . $year . '-' . $month . '-' . $day . ')';

         return;
     }
     echo 'Blogpost ' . htmlentities($slug) . ' detail (' . $year . '-' . $month . '-' . $day . ')';
 });

 // Subrouting
 $router->mount('/movies', function () use ($router) {

     // will result in '/movies'
     $router->get('/', function () {
         echo 'movies overview';
     });

     // will result in '/movies'
     $router->post('/', function () {
         echo 'add movie';
     });

     // will result in '/movies/id'
     $router->get('/(\d+)', function ($id) {
         echo 'movie id ' . htmlentities($id);
     });

     // will result in '/movies/id'
     $router->put('/(\d+)', function ($id) {
         echo 'Update movie id ' . htmlentities($id);
     });
 });

 // Thunderbirds are go!
 $router->run();
