var filmovi = (function($) {
    "use strict";

    let upit = '';
    let podaci = '';
    let odgovor = '';
    const film = 'filmovi';
    const serije = 'serije';
    const crtici = 'crtici';

    const prikaz = 'prikazi';
    const uredi = 'uredi';
    const unesi = 'unesi';

      return {
          pokreni: function() {
            this.sidemenu();
       //     this.unesiSamPodatke();
          },


          sidemenu: function(){
            $('.main-navbar').on('click', 'a', function(e){

              
               let trenutno = $(this).attr('class');
               let grupa = $(this).attr('data-original-title');

                $('.page-header h1').empty();
            
               console.log(trenutno);
                console.log(grupa);
              //  console.log(this);

                if(grupa === film){
                    if(trenutno === prikaz){
                        $('.page-header h3').text('Pregled filmova');
                        
                    }
                    if(trenutno === uredi){
                        upit = 'uredi_medij';
                        $('.page-header h3').text('Popis filmova');
                        filmovi.templateLoader('glavni_dashboard','glavni_dashboard');
                    }
                    if(trenutno === unesi){
                        $('.page-header h3').text('Unesi novi film');
                        console.log('radi');
                        filmovi.formaUnosa('unos_medija','filmovi');
                        filmovi.prijedlogunosa();
                        filmovi.unesiImdb();
                    }
                }
            });

          },


          prijedlogunosa: function(){

            $( ".kontent" ).on( "blur",'#naziv',function( event ) {

                let imefilma = $(this).val();
                let duljina = imefilma.length;

                console.log(this);
                console.log(imefilma);
                $('.ajax').empty();
                $('.ajax').slideDown(300);
                $('.imdb-id').val('0');
                if(duljina > 3){
                  $('.ajax').append('<div class="loader-box"><div class="loader-7"></div></div>');
                  upit = 'imdb';
                  podaci = {
                    ime_filma: imefilma,
                    vrsta_upita: 'imdb_prikaz',
                    link_filma: '',
                    vrsta: '',
                    imdb: ''
                  }
                  filmovi.ajaxapicall(podaci,upit).then(function(odg) {
  
                    $('.loader-box').remove();
                    $('.ajax').html(odg)
                  }).catch(function(err) {
                    // Run this when promise was rejected via reject()
                    console.log(err)
                  })
                }
           });

          },





          // Učitavanje forme za unos, filmovi, serije, crtici
          formaUnosa: function(akcija,vrsta){

            podaci = {
              vrsta_upita: akcija
            }
            filmovi.ajaxapicall(podaci,vrsta).then(function(odg) {
              console.log(odg);
              $('.kontent').html(odg);

            }).catch(function(err) {
              console.log(err);
            }) 

          },

          templateLoader: function(akcija,vrsta){

            podaci = {
              vrsta_upita: akcija
            }
            filmovi.ajaxapicall(podaci,vrsta).then(function(odg) {
             // console.log(odg);
              $('.kontent').html(odg);

            }).catch(function(err) {
              console.log(err);
            }) 

          },





          ajaxhandler: function(podaci,upit){

            filmovi.ajaxapicall(podaci,upit).then(function(odg) {
              console.log(odg);
              return odg;
            }).catch(function(err) {
              return err;
            })

          },

          ajaxapicall: function(podaci,upit){
            return new Promise(function(resolve, reject) {
              $.ajax({
                type: "POST",
                url: 'ajaxapi',
                data: {podaci,upit},
                success: function(odg) {
                  setTimeout(() => {  resolve(odg); }, 300);
                },
                error: function(err) {
                  reject(err) 
                }
              });
            });
          },

          unesiImdb: function(){
            $( ".kontent" ).on( "click",'.add-imdb',function( event ) {
                // Kod upisa imena filma pokreni loader animaciju
                $(this).append('<div class="loader-box"><div class="loader-7"></div></div>');
                // Dohvati ime filma
                let ime_filma = $(this).find('h3').text();
                // Dohvati ID filma
                let imdb_data = $(this).attr('data-imdb');
                // Postavi odabrani naziv filma u input polje
                $('#naziv').val(ime_filma);

                $('.loader-box').remove();
                
                if(imdb_data == '0'){
                    upit = 'imdb';
                    podaci = {
                        ime_filma: ime_filma,
                        vrsta_upita: 'imdb_unos',
                        link_filma: '',
                        vrsta: '',
                        imdb: ''
                      }
                    filmovi.ajaxapicall(podaci,upit).then(function(odg) {
                        $('.loader-box').remove();
                        console.log(odg);
                        let ajaxodgovor = JSON && JSON.parse(odg) || $.parseJSON(odg);
                        $('.imdb-id').val(ajaxodgovor.imdb_id);
                      }).catch(function(err) {
                        // Run this when promise was rejected via reject()
                        console.log(err)
                      })
                      $('.ajax').slideUp(1000);
                    console.log('za unos u bazu');
                } else {
                    $('.ajax').slideUp(1000);
                  
                    $('.imdb-id').val(imdb_data);
                    console.log('nije za unos u bazu');
                }
           });
          },

          unosfilma: function(){
            $( ".filmovi-container" ).on( "submit",'.unos',function( event ) { 

                event.preventDefault();

                // Onemoguči gumb da se ne spama baza
               // $(this).find(':submit').attr('disabled',true);
                $('#kreni').prop("disabled", true);

                // dohvati glavni imdb id za filma
                let imdb_id = $('.imdb-id').val();

                // ako ne postoji glavni imdb id tj. 0 onda prvo dodaj imdb film u bazu
                if(imdb_id == '0'){

                   // Dohvati imdb id iz baze
                   let imdb_id_attr = $('.ajax .add-imdb').attr('data-imdb');
        
                   // ako ne postoji imdb id u bazi onda dodaj sa stranice imdb.com
                   if(imdb_id_attr == '0'){

                    // Dohvati naziv sluzbeni naziv filma
                    let imdb_attr_ime = $('.ajax .add-imdb h3').text();

                    // Dodaj službeni naziv filma u input polje, ako korisnik je krivo napisao filma
                    $('#naziv').val(imdb_attr_ime);

                    upit = 'dodaj_imdb_u_bazu';

                    podaci = {
                        ime_filma: imdb_attr_ime,
                        link_filma: '',
                        vrsta: 'json',
                        imdb: ''
                      }

                      // Ajax za dodavanje naziv imdb_filmova u bazu
                    filmovi.ajaxapicall(podaci,upit).then(function(odg) {
                        $('.loader-box').remove();
                        // dok uspješno dodamo imdb film u bazu dohvati id iz baze
                        let ajaxodgovor = JSON && JSON.parse(odg) || $.parseJSON(odg);
                        $('.imdb-id').val(ajaxodgovor.imdb_id);
                        // Pokreni funkciju za dodavanje filma u bazu
                        filmovi.dodaj_ga();
                      }).catch(function(err) {
                        // Run this when promise was rejected via reject()
                        console.log(err)
                      })
                   } else {

                        // Ako korisnik nije kliknuo na film a film imdb postoji u bazi, dohvati id i nastavi sa unosom filma
                        console.log('Korisnik nije kliknuo na film pa uzmi prvi id');
                        let postojeci_id = $('.ajax .add-imdb:first-child').attr('data-imdb');
                        $('.imdb-id').val(postojeci_id);
                        filmovi.dodaj_ga();
                        console.log(postojeci_id);
                   }
                   console.log(imdb_id);

                    console.log('nemogu unjeti film');
                } else {

                    console.log('postoji imdb id i nastavi sa unosom');
                    // Ako je korisnik kliknuo na film za unos i postoji glavni imdb id, dodaj film
                    filmovi.dodaj_ga();

                    console.log('Spreman za unos filma');
                }

             });

          },

/*
          
          promjeniStatus: function(){

            upit = 'promjena_statusa';
            
            $('.kontent').on('click', '.switch input', function(){
              console.log('klik');
              let status = '';
              let media_id = $(this).attr('media-id')


              if ($(this).prop('checked')) {
                console.log('checked 1');
                 status = '1';
            } else {
               status = '0';
            }

            podaci = {
              status_medija: status,
              medija_id: media_id
            }

            console.log(podaci);

            filmovi.ajaxapicall(podaci,upit).then(function(odg) {

              console.log(odg);
             
            }).catch(function(err) {
              // Run this when promise was rejected via reject()
              console.log(err)
            })


            });


          },


          tost: function(){
            
          },






          dodaj_ga: function(){

           let imefilma = $('#naziv').val();
           let linkfilma = $('#link-photo').val();
           let imdb_id = $('.imdb-id').val();
           let upit = 'filmovi';
            podaci = {
                ime_filma: imefilma,
                link_filma: linkfilma,
                vrsta: 'html',
                imdb: imdb_id
              }

              filmovi.ajaxapicall(podaci,upit).then(function(odg) {
                $('.loader-box').remove();
                console.log(odg);
                $('#kreni').prop("disabled", false);
                $('.odgovor').html(odg);
                $('.odgovor').slideDown();
                //$(".timed-div").delay(5000).fadeOut();
             //   let ajaxodgovor = JSON && JSON.parse(odg) || $.parseJSON(odg);
             //   $('.imdb-id').val(ajaxodgovor.imdb_id);
              }).catch(function(err) {
                // Run this when promise was rejected via reject()
                console.log(err)
              })

          },

          unosfilma: function(){
            $( ".filmovi-container" ).on( "submit",'.unos',function( event ) { 

                event.preventDefault();

                // Onemoguči gumb da se ne spama baza
               // $(this).find(':submit').attr('disabled',true);
                $('#kreni').prop("disabled", true);

                // dohvati glavni imdb id za filma
                let imdb_id = $('.imdb-id').val();

                // ako ne postoji glavni imdb id tj. 0 onda prvo dodaj imdb film u bazu
                if(imdb_id == '0'){

                   // Dohvati imdb id iz baze
                   let imdb_id_attr = $('.ajax .add-imdb').attr('data-imdb');
        
                   // ako ne postoji imdb id u bazi onda dodaj sa stranice imdb.com
                   if(imdb_id_attr == '0'){

                    // Dohvati naziv sluzbeni naziv filma
                    let imdb_attr_ime = $('.ajax .add-imdb h3').text();

                    // Dodaj službeni naziv filma u input polje, ako korisnik je krivo napisao filma
                    $('#naziv').val(imdb_attr_ime);

                    upit = 'dodaj_imdb_u_bazu';

                    podaci = {
                        ime_filma: imdb_attr_ime,
                        link_filma: '',
                        vrsta: 'json',
                        imdb: ''
                      }

                      // Ajax za dodavanje naziv imdb_filmova u bazu
                    filmovi.ajaxapicall(podaci,upit).then(function(odg) {
                        $('.loader-box').remove();
                        // dok uspješno dodamo imdb film u bazu dohvati id iz baze
                        let ajaxodgovor = JSON && JSON.parse(odg) || $.parseJSON(odg);
                        $('.imdb-id').val(ajaxodgovor.imdb_id);
                        // Pokreni funkciju za dodavanje filma u bazu
                        filmovi.dodaj_ga();
                      }).catch(function(err) {
                        // Run this when promise was rejected via reject()
                        console.log(err)
                      })
                   } else {

                        // Ako korisnik nije kliknuo na film a film imdb postoji u bazi, dohvati id i nastavi sa unosom filma
                        console.log('Korisnik nije kliknuo na film pa uzmi prvi id');
                        let postojeci_id = $('.ajax .add-imdb:first-child').attr('data-imdb');
                        $('.imdb-id').val(postojeci_id);
                        filmovi.dodaj_ga();
                        console.log(postojeci_id);
                   }
                   console.log(imdb_id);

                    console.log('nemogu unjeti film');
                } else {

                    console.log('postoji imdb id i nastavi sa unosom');
                    // Ako je korisnik kliknuo na film za unos i postoji glavni imdb id, dodaj film
                    filmovi.dodaj_ga();

                    console.log('Spreman za unos filma');
                }

             });

          },


          unostimdb: function(){
            $( ".filmovi-container" ).on( "click",'.add-imdb',function( event ) {

                // Kod upisa imena filma pokreni loader animaciju
                $(this).append('<div class="loader-box"><div class="loader-7"></div></div>');
                
                // Dohvati ime filma
                let ime_filma = $(this).children('h3').text();
                // Dohvati ID filma
                let imdb_data = $(this).attr('data-imdb');
                // Postavi odabrani naziv filma u input polje
                $('#naziv').val(ime_filma);

                $('.loader-box').remove();
                
                if(imdb_data == '0'){
                    upit = 'dodaj_imdb_u_bazu';
                    podaci = {
                        ime_filma: ime_filma,
                        link_filma: '',
                        vrsta: '',
                        imdb: ''
                      }

                    filmovi.ajaxapicall(podaci,upit).then(function(odg) {
                        $('.loader-box').remove();
                        console.log(odg);
                        let ajaxodgovor = JSON && JSON.parse(odg) || $.parseJSON(odg);
                        $('.imdb-id').val(ajaxodgovor.imdb_id);
                      }).catch(function(err) {
                        // Run this when promise was rejected via reject()
                        console.log(err)
                      })
                      $('.ajax').slideUp(1000);
                    console.log('za unos u bazu');
                } else {
                    $('.ajax').slideUp(1000);
                  
                    $('.imdb-id').val(imdb_data);
                    console.log('nije za unos u bazu');
                }


    
                
               
               
           });
          },


          prijedlogfilma: function(){

            $( ".filmovi-container" ).on( "blur",'#naziv',function( event ) {

                let imefilma = $(this).val();
                let duljina = imefilma.length;
                
                $('.ajax').empty();
                $('.ajax').slideDown(300);
                $('.imdb-id').val('0');
                if(duljina > 3){
                  $('.ajax').append('<div class="loader-box"><div class="loader-7"></div></div>');
                  upit = 'imdb';
                  podaci = {
                    ime_filma: imefilma,
                    link_filma: '',
                    vrsta: '',
                    imdb: ''
                  }
                  filmovi.ajaxapicall(podaci,upit).then(function(odg) {
  
                    $('.loader-box').remove();
                    $('.ajax').html(odg)
                  }).catch(function(err) {
                    // Run this when promise was rejected via reject()
                    console.log(err)
                  })
                }
           });

          },

          ajaxapicall: function(podaci,upit){

            return new Promise(function(resolve, reject) {
              $.ajax({
                type: "POST",
                url: 'ajaxapi',
                data: {podaci,upit},
                success: function(odg) {

                  // Simulira traženje u bazi, čeka 2 sekunde
                  setTimeout(() => {  resolve(odg); }, 300);

                 // resolve(odg);
                   // Resolve promise and go to then()
                },

                error: function(err) {

                  reject(err) // Reject the promise and go to catch()
                  
                }
              });
            });
          },
*/

}/* Return end dont pass */
  
})( jQuery );
  
jQuery(document).ready(function() {
  filmovi.pokreni();
});