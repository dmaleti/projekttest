<header class="main-nav">
          <div class="logo-wrapper"><a href="<?php url_stranice(); ?>administracija" data-original-title="" title=""><img class="img-fluid for-light" src="<?php admin_assets(); ?>/img/logo.png" alt="" data-original-title="" title=""></a>      
          </div>
          
          <nav>
            <div class="main-navbar">
              <div class="left-arrow disabled" id="left-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar" style="display: block;">
                  <li class="back-btn"><a href="index.html" data-original-title="" title=""><img class="img-fluid" src="<?php admin_assets(); ?>/img/logo.png" alt="" data-original-title="" title=""></a>
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="sidebar-title">
                    <div>
                      <h6 class="lan-1">General</h6>
                      <p class="lan-2">Dashboards,widgets &amp; layout.</p>
                    </div>
                  </li>
                  <li class="dropdown"><a class="prikazi nav-link menu-title"  data-original-title="filmovi" title=""><i data-feather="video"></i><span class="svi">Filmovi</span>
                      <div class="according-menu"><i class="fa fa-angle-right"></i></div></a>
                    <ul class="nav-submenu menu-content" style="display: none;">
                      <li><a class="uredi"  data-original-title="filmovi" title="">Svi filmovi</a></li>
                      <li><a class="unesi"  data-original-title="filmovi" title="">Dodaj film</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg><span class="lan-6">Widgets</span><div class="according-menu"><i class="fa fa-angle-right"></i></div></a>
                    <ul class="nav-submenu menu-content" style="display: none;">
                      <li><a href="general-widget.html" data-original-title="" title="">General</a></li>
                      <li><a href="chart-widget.html" data-original-title="" title="">Chart</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></div>
            </div>
          </nav>
        </header>