<?php

include_once PUTANJA . 'app/dbz.php';
class Korisnici {

    public $id;

    public function __construct(){
      
    }
   
    public function korisnik($id){
        $this->id = $id;
        $this->korisnik = new dbz();
        $this->korisnik->imeTablice('korisnici');
        $this->podaci = $this->korisnik->findOne((['id' => $id]));
        $this->ime = $this->podaci['korisnicko_ime'];
    }

    public function korisnik_meta_pdaci($id){
        $this->id = $id;
        $this->korisnik = new dbz();
        $this->korisnik->imeTablice('korisnicki_podaci');
        $this->podaci = $this->korisnik->korisnicki_meta_podaci($id,'avatar');
        
        $this->avatar = $this->podaci['0']['vrijednost_podatka'];
    }

}

