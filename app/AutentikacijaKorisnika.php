<?php
include_once 'postavke.php';
class AutentikacijaKorisnika {

    
   private $korisnicko_ime = '';
   private $email= '';
   private $lozinka= '';
   private $potvrda_lozinke= '';
   private $funkcija = '';
   private $korisnicki_id = '';

   
    public function __construct(){
        $this->korisnik = new dbz();
        $this->korisnik->imeTablice('korisnici');
        $this->errorPoruke();
        $this->greske = 0;
       
       }

       public function procesiarj($pod,$fun){

         if($fun == 'registrirajKorisnika'){

            $this->funkcija        = 'registrirajKorisnika';
            $this->korisnicko_ime  = $pod['podaci']['kor_ime'];
            $this->email           = $pod['podaci']['kor_email'];
            $this->lozinka         = $pod['podaci']['kor_password'];
            $this->potvrda_lozinke = $pod['podaci']['kor_potvrda'];
            $this->handler_greski();
         }

         if($fun == 'prijavaKorisnika'){

            $this->funkcija        = 'prijavaKorisnika';
            $this->korisnicko_ime  = $pod['podaci']['kor_ime'];
          //  $this->email           = $pod['podaci']['kor_email'];
            $this->lozinka         = $pod['podaci']['kor_password'];
            $this->handler_greski();
         }


       }

       private function errorPoruke(){
         $this->prazno_korisnicko_ime =       'Unesite korisničko ime';
         $this->zauzeto_korisnicko_ime =      'To je korisničko ime zauzeto. Pokušajte s nekim drugim.';
         $this->nedozvoljeni_znakovi_ime =    'Žao nam je, samo slova(a-z), brojevi (0-9) su dozvoljeni.';
         $this->max_znakovi_ime =              'Korisničko ime mora biti imati između 3 i 30 znakova.';
         $this->korisnicko_ime_neposotoji =    'Korisničko ime ne postoji';

         $this->prazan_email =                 'Unesite email';
         $this->nedozvoljena_mail_adresa =     'Nevažeća email adresa';
         $this->zauzeta_mail_adresa =           'Email se već koristi';
         $this->prekratka_zaporka =             'Zaporka treba biti min 8 znakova';
         $this->zaporka_nepodudara =            'Zaporka se ne podudara';

         $this->kriva_zaporka =                 'Zaporka je kriva';

       }

       public function test(){
       
          $this->provjeriKorIme();
       }



      public function provjeriKorIme(){

         if(!isset($this->korisnicko_ime)){
            return;
         }

         $ime = trim($this->korisnicko_ime);

         if(empty($ime)){
            return $this->prazno_korisnicko_ime;
         }

         if(strlen($ime) < 3 || strlen($ime) > 30){
            return $this->max_znakovi_ime;
         }

         if(!ctype_alnum($ime)){
            return $this->nedozvoljeni_znakovi_ime;
         }

         if($this->funkcija === 'registrirajKorisnika'){
            $ime = $this->korisnik->findOne((['korisnicko_ime' => $ime]));
            if($ime){
               return $this->zauzeto_korisnicko_ime;
            } else {
               return true;
            }
         }

         if($this->funkcija === 'prijavaKorisnika'){
            $ime = $this->korisnik->findOne((['korisnicko_ime' => $ime]));
            if(!$ime){
               return $this->korisnicko_ime_neposotoji;
            } else {




               return true;
            }
         }
        
         return true;
            
     }



     private function provjeriKorEmail(){

      if(!isset($this->email )){
         return;
      }

      $email = trim($this->email);
   
      if(empty($email)){
          return $this->prazan_email;
      }
      
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         return $this->nedozvoljena_mail_adresa;
       }


       $email = $this->korisnik->findOne((['email' => $email]));
       if($email){
         return $this->zauzeta_mail_adresa;
       } 
       return true;
       
   }

   private function provjeriKorLozinku(){

      if (strlen($this->lozinka) < 1 || strlen($this->lozinka) > 32) {

          return $this->prekratka_zaporka;

       } elseif($this->lozinka != $this->potvrda_lozinke && $this->funkcija === 'registrirajKorisnika'){

          return $this->zaporka_nepodudara;

       } elseif ($this->funkcija === 'prijavaKorisnika'){


         if( is_bool ($this->provjeriKorIme() )) {

            $hashed_lozinka = $this->korisnik->findOne((['korisnicko_ime' => $this->korisnicko_ime]));
            if($hashed_lozinka){
               $this->korisnicki_id = $hashed_lozinka['id'];
               $lozinka = password_verify($this->lozinka, $hashed_lozinka['lozinka']);
             }
             if(!$lozinka){
            
               return $this->kriva_zaporka;
             } else {
               
               return true;
             }
         }

       } else {

         return true;

       }
  
   }

   private function korisnicki_meta_podaci($id){
      $this->korisnik->imeTablice('korisnicki_podaci');
      $this->korisnik->dodaj([
         'korisnik_id' => $id,
         'kljuc_podataka' => 'avatar',
         'vrijednost_podatka' => URLSTR . 'public/assets/image/avatar.png',
       ]);

   }

   public function handler_greski(){

      $kor_ime = 0;
      $kor_email = 0;
      $kor_lozinka = 0;
  
      $kor_ime_greska = '';
      $kor_email_greska = '';
      $kor_lozinka_greska = '';



      if($this->funkcija === 'registrirajKorisnika' && is_bool($this->provjeriKorIme()) && is_bool($this->provjeriKorEmail()) && is_bool($this->provjeriKorLozinku())){
         if($this->korisnik->dodaj([
            'korisnicko_ime' => $this->korisnicko_ime,
            'email' => $this->email,
            'lozinka' => password_hash($this->lozinka, PASSWORD_DEFAULT),
            'aktivacijski_kljuc' => substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(32))), 0, 32),
            'status' => '0',
          ]
          ))
          {
              echo json_encode(array('usjesna_registracija' => 'Dogodila se greška, molimo pokušajte kasnije'));
              
          } else {
            $zadnji_id = $this->korisnik->lastSavedId();
            $this->korisnicki_meta_podaci($zadnji_id);
              echo json_encode(array('usjesna_registracija' => 1));
          }

      } elseif ($this->funkcija === 'prijavaKorisnika' && is_bool($this->provjeriKorIme()) && is_bool($this->provjeriKorLozinku())){

      

         $sesija = new sesija();
         $sesija->postavi_podatke_sesije($this->korisnicki_id,$this->korisnicko_ime);

         

         echo json_encode(array('uspjesna_prjava' => 1));

        
         
        
      }else {
                     if(!empty($this->provjeriKorIme()) && !is_string($this->provjeriKorIme())){

                        $kor_ime = 1;

                  } else {

                     $kor_ime_greska = $this->provjeriKorIme();

                  }

                  
                  if(!empty($this->provjeriKorEmail()) && !is_string($this->provjeriKorEmail())){

                     $kor_email = 1;

               } else {

                  $kor_email_greska = $this->provjeriKorEmail();

               }
                
               if(!empty($this->provjeriKorLozinku()) && !is_string($this->provjeriKorLozinku())){

                  $kor_lozinka = 1;

            } else {

               $kor_lozinka_greska = $this->provjeriKorLozinku();

            }

               echo json_encode(array('username' => $kor_ime, 'username_err' => $kor_ime_greska, 'email' => $kor_email, 'email_err' => $kor_email_greska, 'lozinka' => $kor_lozinka, 'lozinka_err' => $kor_lozinka_greska));
          
      }

    }
}