var korisnici = (function($) {
  "use strict";
  
  let initForma = '';
  let hideForma = '';
  let sljedecaForma = '';
  let treForma = '';
  let ime = '';
  let lozinka = '';
  let email = '';
  let conf_pass = '';
  let username_err = 1;
  let email_err = 1;
  let lozinka_err = 1;
  let confirm_password_err = 1;
	return {
		pokreni: function() {
     
            this.forma();
            this.registracija();
            this.prijava();
           // this.logmeout();
        },

        logmeout: function(){

$('.odjava').on('click', function(e){

  let podaci = 'logout';
  let upit = 'logmeout';


console.log('test');
  $.ajax({
    type: "POST",
    url: 'ajaxapi',
    data: {podaci,upit},

    success: function(response)
    {
      
      
      

       }
       
   });


});



         
        },

        forma: function(){

          let url = window.location.pathname;
          let link = url.split('/').pop().replace(/\s*/g,'');
          if(link == '' || link == 'prijava'){
            $('.prijava-forma').fadeIn();
            initForma = 'prijava';
          } else {
            $('.prijava-forma').fadeOut();
          }
          if(link == 'registracija'){
            initForma = 'registracija';
            $('.registracijska-forma').fadeIn();
          }else {
            $('.registracijska-forma').fadeOut();
          }
          if(link == 'resetiraj-lozinku'){
            initForma = 'reset';
            $('.resetiraj-forma').fadeIn();
          }else {
            $('.resetiraj-forma').fadeOut();
          }

          let treForma = initForma;
          
          $('a').on('click', function(e){
            e.preventDefault();
            let formID = $(this).attr('data-form');
                initForma = $(this).closest('form').attr('class');
                hideForma = $("."+initForma).closest('section').attr('class');
                sljedecaForma = $(".autentikacija").find("[data-default="+formID+"]").attr('class');
                treForma = $("."+sljedecaForma).find('form').attr('class');
            $("."+hideForma).slideUp();
            $("."+sljedecaForma).delay(200).slideDown();
           
          });

         
          $('input').on('focus', function(){
            $(this).removeClass('uspjesno');
            $(this).removeClass('neuspjesno');
           });
         

        },



        prijava: function(){

          $('.prijava input').on('blur', function(){
            let vrijednost = $(this).val();
            let polje = $(this).attr('name');
            let poljeid = $(this).attr('id');
            console.log(vrijednost);

            if(polje === 'username'){

              let usernames = korisnici.korisnickoIme(polje,vrijednost)
              korisnici.handlerGreski(poljeid,usernames);
            }

            if(polje === 'password'){
              let lozinkas = korisnici.korisnickaLozinka(polje,vrijednost)
              korisnici.handlerGreski(poljeid,lozinkas);
              }
          });

          $('.prijava').submit(function(e) {
            e.preventDefault();

         if(!ime && !lozinka){
         $(this).find('input').trigger("blur");
            return;
         } 

        if(username_err === 0 && lozinka_err === 0){
          let $test = {
            kor_ime: ime,
            kor_password: lozinka,
          }
      korisnici.ajaxProvjera($test,'prijavaKorisnika');

         }

          
          });

        },





        registracija: function(){

          $('.registracija input').on('blur', function(){
            let vrijednost = $(this).val();
            let polje = $(this).attr('name');
            let poljeid = $(this).attr('id');
            console.log(vrijednost);

            if(polje === 'username'){

              let usernames = korisnici.korisnickoIme(polje,vrijednost)
              korisnici.handlerGreski(poljeid,usernames);
            }

            if(polje === 'email'){
            let emails = korisnici.korisnickiEmail(polje,vrijednost)
            korisnici.handlerGreski(poljeid,emails);
            }
            if(polje === 'password' || polje === 'confirm_password'){
              let lozinkas = korisnici.korisnickaLozinka(polje,vrijednost)
              korisnici.handlerGreski(poljeid,lozinkas);
              }
          });
         
   
          $('.registracija').submit(function(e) {
            e.preventDefault();

         if(!ime && !email && !lozinka && !conf_pass){
          $(this).find('input').trigger("blur");
            return;
         } 

     
        if(username_err === 0 && email_err === 0 && lozinka_err === 0 && confirm_password_err === 0){
          let $test = {
            kor_ime: ime,
            kor_email: email,
            kor_password: lozinka,
            kor_potvrda: conf_pass,
          }
        korisnici.ajaxProvjera($test,'registrirajKorisnika');

         }

          
          });
        },

        ajaxProvjera: function(podaci,upit) {

          var getUrl = window.location;
          var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
          $.ajax({
            type: "POST",
            url: 'ajaxapi',
            data: {podaci,upit},
      
            success: function(response)
            {
              
               console.log(response);
              var data = JSON && JSON.parse(response) || $.parseJSON(response);

              if(data.usjesna_registracija){
                $('.registracijska-forma').fadeOut();
                $('.uspjeh').text('Uspješno ste se registrirali');
                window.location.href = baseUrl+"/prijava";
                return;
               }
               if(data.uspjesna_prjava){
                window.location.href = baseUrl+"/administracija";
               }
               if(upit == 'registrirajKorisnika' && data.username == '0'){
                $('#reg-username').removeClass('uspjesno');
                $('#reg-username').addClass('neuspjesno');
                $('#reg-username').siblings('.help-block').text(data.username_err).fadeIn("slow");
               } else {
                $('#reg-username').removeClass('neuspjesno');
                $('#reg-username').addClass('uspjesno');
                $('#reg-username').siblings('.help-block').fadeOut("slow");
               }
               
               if(upit == 'registrirajKorisnika' && data.email == '0'){
                $('#reg-email').removeClass('uspjesno');
                $('#reg-email').addClass('neuspjesno');
                $('#reg-email').siblings('.help-block').text(data.email_err).fadeIn("slow");
               } else {
                $('#reg-email').removeClass('neuspjesno');
                $('#reg-email').addClass('uspjesno');
                $('#reg-email').siblings('.help-block').fadeOut("slow");
               }

               if(upit == 'prijavaKorisnika' && data.username == '0'){
                $('#log-username').removeClass('uspjesno');
                $('#log-username').addClass('neuspjesno');
                $('#log-username').siblings('.help-block').text(data.username_err).fadeIn("slow");
               } else {
                $('#log-username').removeClass('neuspjesno');
                $('#log-username').addClass('uspjesno');
                $('#log-username').siblings('.help-block').fadeOut("slow");
               }

               if(upit == 'prijavaKorisnika' && data.lozinka == '0'){
                $('#log-password').removeClass('uspjesno');
                $('#log-password').addClass('neuspjesno');
                $('#log-password').siblings('.help-block').text(data.lozinka_err).fadeIn("slow");
               } else {
                $('#log-password').removeClass('neuspjesno');
                $('#log-password').addClass('uspjesno');
                $('#log-password').siblings('.help-block').fadeOut("slow");
               }

               }

               
               
           });
    
        },


        korisnickaLozinka: function(polje,vrijednost){

          let greska = {
            klasa: 'uspjesno',
            greska: ''
          };

          if(polje === 'password'){
            if(!vrijednost){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Molimo upišite lozinku'
              };
              return greska;
            }

            if(vrijednost.length <= 2){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Lozinka mora sadržavati najmanje 8 znakova'
              };
              return greska;
            }
            lozinka_err = 0;
            lozinka = vrijednost;
            return greska;
          }

          if(polje === 'confirm_password'){

            if(!vrijednost){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Molimo upišite lozinku'
              };
              return greska;
            }

            if(lozinka !== vrijednost){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Lozinke se ne podudaraju'
              };
              
              return greska;
            }
            confirm_password_err = 0;
            conf_pass = vrijednost;
            return greska;
          }



        },


        korisnickiEmail: function(polje,vrijednost){

          if(polje === 'email'){

            let greska = {
              klasa: 'uspjesno',
              greska: ''
            };

            if(!vrijednost){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Upišite email'
                };
                return greska;
            }
            let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if(!regex.test(vrijednost)) {

        
              return greska = {
                klasa: 'neuspjesno',
                greska: 'Upišite ispravnu email adresu'
              };
            }
            email_err = 0;
            email = vrijednost;
            return greska;


            }else {
              return;
            }

              },




        korisnickoIme: function(polje,vrijednost){


          
          if(polje === 'username'){

            let greska = {
              klasa: 'uspjesno',
              greska: ''
            };
            
            if(!vrijednost){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Korisničko ime je obavezno'
                };
                
                return greska;
            }
            
            if(vrijednost.length <= 2 ){
             greska = {
              klasa: 'neuspjesno',
              greska: 'Korisničko ime treba imati najmanje 3 slova'
              };
              return greska;
            }
  
            if(vrijednost.length <= 2  || vrijednost.length >= 32){
              greska = {
               klasa: 'neuspjesno',
               greska: 'Korisničko ime treba imati najvise 32 slova'
               };
               return greska;
             }
  
            let dopusteno = /^[0-9a-zA-Z]+$/;
            if(!vrijednost.match(dopusteno)){
              greska = {
                klasa: 'neuspjesno',
                greska: 'Korisničko ime nesmije sadržavati specijalne znakove'
                };
                return greska;
            }
            username_err = 0;
            ime = vrijednost;
            return greska;
            
          } else {
            return;
          }
          
        },

        handlerGreski: function(polje,greska){
          if(greska.greska){
            $("#"+polje).addClass(greska.klasa);
            $("#"+polje).siblings('.help-block').text(greska.greska).fadeIn("slow");
           } else{
            $("#"+polje).addClass(greska.klasa);
            $("#"+polje).siblings('.help-block').fadeOut("slow");
         //   korisnicki_pt_password = vrijednost;
         //   confirm_password_err = 0;
           }

        },

}/* Return end dont pass */

})( jQuery );

jQuery(document).ready(function() {
  
	korisnici.pokreni();
});