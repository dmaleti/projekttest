
<!DOCTYPE html>
<html lang="hr">
<head>
    <meta charset="UTF-8">
    <title>Registracija</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/assets/css/korisnici.css">
</head>
<body>

<div class="flex-container">

    <div class="autentikacija flex-item">

    <div class="uspjeh"></div>
    <section class="registracijska-forma" data-default="0">
    
    <div class="title-items">
    <h2>Registracija</h2>
    </div>
                
        <form class="registracija" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username</label>
                <input id="reg-username" type="text" name="username" class="form-control" value="">
                <span style="display:none;" class="help-block"></span>
            </div> 
            <div class="form-group">
                <label>Email</label>
                <input id="reg-email" type="text" name="email" class="form-control" value="">
                <span class="help-block"></span>
            </div>     
            <div class="form-group">
                <label>Password</label>
                <input id="reg-password" type="password" name="password" class="form-control" value="">
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input id="reg-confirm_password" type="password" name="confirm_password" class="form-control" value="">
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <input id="reg-submit" type="submit" class="btn btn-primary" value="Submit">
            </div>
            <p>Več imate račun? <a class="btn-prijava" href="/projekt2/prijava" data-form="1">Prijavi se</a>.</p>
        </form>
       
    </section>    

    <section class="prijava-forma" data-default="1">
    <div class="title-items">
    <h2>Prijava</h2>
    </div>
    <form class="prijava" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username/email</label>
                <input id="log-username" type="text" name="username" class="form-control" value="" autocomplete="on">
                <span style="display:none;" class="help-block"></span>
            </div>    
            <div class="form-group">
                <label>Password</label>
                <input id="log-password" type="password" name="password" class="form-control" value="" autocomplete="on">
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <input id="log-submit" type="submit" class="btn btn-primary" value="Submit">
            </div>
            <p>Niste registrirani? <a class="btn-registracija" href="/projekt2/registracija" data-form="0">Registriraj se</a>.</p>
            <a class="btn-resetiraj" href="/projekt2/resetiraj-lozinku" data-form="2">Zaboravljena lozinka?</a>
        </form>
    </section>

    <section class="resetiraj-forma" data-default="2">
    <div class="title-items">
    <h2>Resetiraj lozinku</h2>
    </div>
    <form class="reset" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username/email</label>
                <input id="res-username" type="text" name="username" class="form-control" value="" autocomplete="on">
                <span style="display:none;" class="help-block"></span>
            </div>
            <div class="form-group">
                <input data-form="1" id="res-submit" type="submit" class="btn btn-primary" value="Submit">
            </div>
            <p>Niste registrirani? <a class="btn-registracija" href="/projekt2/registracija" data-form="0">Registriraj se</a>.</p>
            <p>Več imate račun? <a class="btn-prijava" href="/projekt2/prijava" data-form="1">Prijavi se</a>.</p>
        </form>
    </section>




    </div>
</div>  



    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="public/assets/js/korisnici.js"></script>

</body>
</html>