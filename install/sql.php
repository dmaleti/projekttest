<?php

class unosutablu {
    private $dbuser = 'root';
    private $dblozinka = '';
    private $dbbaza = 'projekt';
    private $dbhost = 'localhost';

    private $parameters;

    private $stmt;
    private $data = array();
    private $sql;
    private $where;
    private $fields;
    private $count;
    private $fetch;
    private $lastId;


    public function __construct()
    {
        $this->Connect();
        $this->parameters = array();
        $this->createusers();
        $this->korisnickipodaci();
        $this->createfilmovi();
        $this->createserije();
        $this->createcrtici();
        $this->podaci_filmova();
        $this->podaci_serija();
        $this->podaci_crtica();
        $this->imdb_filmovi();
    }



    public function Connect()
    {
        $dsn = 'mysql:dbname=' . $this->dbbaza . ';host=' . $this->dbhost . '';
        try {
            # Read settings from INI file, set UTF8
            $this->pdo = new PDO($dsn, $this->dbuser, $this->dblozinka, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));
            
            # We can now log any exceptions on Fatal error. 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            # Disable emulation of prepared statements, use REAL prepared statements instead.
            //$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            
            # Connection succeeded, set the boolean to true.
            $this->dbconnection = true;
           
        }
        catch (PDOException $e) {
            # Write into log
            throw new Exception($e->getMessage());
            
            die();
        }
    }
   
   
  public function createusers(){
   
   // sql to create table
   $sql = "CREATE TABLE korisnici (
       id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       korisnicko_ime varchar(60) NOT NULL,
       email varchar(100) NOT NULL,
       lozinka varchar(255) NOT NULL,
       aktivacijski_kljuc varchar(255) NOT NULL,
       status int(11), 
       posljednja_prijava datetime,
       datum_registracije TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
       )";
     
       // use exec() because no results are returned
       $this->pdo->exec($sql);
       echo "Korisnici su uspješno kreirani";
   }


   public function korisnickipodaci(){
   
    // sql to create table
    $sql = "CREATE TABLE korisnicki_podaci (
        id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        usermeta_id bigint(20),
        korisnik_id bigint(20) NOT NULL,
        kljuc_podataka varchar(100) NOT NULL,
        vrijednost_podatka varchar(255) NOT NULL
        )";
      
        // use exec() because no results are returned
        $this->pdo->exec($sql);
        echo "Korisnici su uspješno kreirani";
    }
   
   public function createfilmovi(){
       // sql to create table
       $sql = "CREATE TABLE filmovi (
           id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
           autor_posta bigint(20) NOT NULL,
           naziv varchar(200),
           goo_link varchar(255),
           hash_link varchar(255),
           slug varchar(200),
           status int(11), 
           datum_uredivanja datetime,
           datum_unosa TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
           )";
         
           // use exec() because no results are returned
           $this->pdo->exec($sql);
           echo "Filmovi su uspješno kreirani";
       }

       public function createserije(){
        // sql to create table
        $sql = "CREATE TABLE serije (
            id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            autor_posta bigint(20) NOT NULL,
            naziv varchar(200),
            goo_link varchar(255),
            hash_link varchar(255),
            slug varchar(200),
            status int(11), 
            datum_uredivanja datetime,
            datum_unosa TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            )";
          
            // use exec() because no results are returned
            $this->pdo->exec($sql);
            echo "Filmovi su uspješno kreirani";
        }

        public function createcrtici(){
            // sql to create table
            $sql = "CREATE TABLE crtici (
                id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                autor_posta bigint(20) NOT NULL,
                naziv varchar(200),
                goo_link varchar(255),
                hash_link varchar(255),
                slug varchar(200),
                status int(11), 
                datum_uredivanja datetime,
                datum_unosa TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                )";
              
                // use exec() because no results are returned
                $this->pdo->exec($sql);
                echo "Filmovi su uspješno kreirani";
            }

       public function podaci_filmova(){
        // sql to create table
        $sql = "CREATE TABLE podaci_filmova (
            id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            film_id bigint(20) NOT NULL,
            kljuc_podataka varchar(100),
            vrijednost_podatka longtext
            )";
          
            // use exec() because no results are returned
            $this->pdo->exec($sql);
            echo "Filmovi su uspješno kreirani";
        }

        public function podaci_serija(){
            // sql to create table
            $sql = "CREATE TABLE podaci_serija (
                id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                serija_id bigint(20) NOT NULL,
                kljuc_podataka varchar(100),
                vrijednost_podatka longtext
                )";
              
                // use exec() because no results are returned
                $this->pdo->exec($sql);
                echo "Filmovi su uspješno kreirani";
            }

            public function podaci_crtica(){
                // sql to create table
                $sql = "CREATE TABLE podaci_crtica (
                    id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    crtic_id bigint(20) NOT NULL,
                    kljuc_podataka varchar(100),
                    vrijednost_podatka longtext
                    )";
                  
                    // use exec() because no results are returned
                    $this->pdo->exec($sql);
                    echo "Filmovi su uspješno kreirani";
                }

        public function imdb_filmovi(){
            // sql to create table
            $sql = "CREATE TABLE imdb_podaci (
                id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                naziv varchar(255),
                slika longtext,
                opis longtext,
                zanr varchar(255),
                plot longtext,
                ocijena varchar(255),
                trajanje varchar(255),
                duljina varchar(255),
                trailer varchar(255),
                godina_izlaska varchar(255),
                broj_sezona varchar(255),
                vrsta varchar(255)
                )";
              
                // use exec() because no results are returned
                $this->pdo->exec($sql);
                echo "Filmovi su uspješno kreirani";
            }


   
}
new unosutablu();