<?php

if ( ! defined( 'PUTANJA' ) ) {
	define( 'PUTANJA',  __DIR__ . '/' );
}

if ( ! defined( 'URLSTR' ) ) {
	define( 'URLSTR', "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER["REQUEST_URI"].'?').'/' );
}

if ( ! defined( 'DOWNLOAD' ) ) {
	define( 'DOWNLOAD',  __DIR__ . '/upload/imdb/' );
}

require_once 'app/postavke.php';